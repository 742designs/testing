var source = require('vinyl-source-stream');
var gulp = require('gulp');
var gutil = require('gulp-util');
var compass = require('gulp-compass');
var browserify = require('browserify');
var babelify = require('babelify');
var watchify = require('watchify');
var notify = require('gulp-notify');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var buffer = require('vinyl-buffer');

var browserSync = require('browser-sync');
var reload = browserSync.reload;

var sassSources = ['sass/style.scss'];

// Compile SASS to CSS dir
gulp.task('compass', function () {
  gulp.src(sassSources)
    .pipe(compass({
      sass: 'sass',
      // image: 'images',
      style: 'compressed'
    }))
    .on('error', gutil.log)
    .pipe(gulp.dest('./css'))
    .pipe(browserSync.stream({match: '**/*.css'}));
});

function handleErrors() {
  var args = Array.prototype.slice.call(arguments);
  notify.onError({
    title: 'Compile Error',
    message: '<%= error.message %>'
  }).apply(this, args);
  this.emit('end'); // Keep gulp from hanging on this task
}

function buildScript(file, watch) {
  var props = {
    entries: ['./js/' + file],
    debug : true,
    cache: {},
    packageCache: {},
    transform:  [babelify.configure({stage : 0 })]
  };

  // watchify() if watch requested, otherwise run browserify() once
  var bundler = watch ? watchify(browserify(props)) : browserify(props);

  function rebundle() {
    var stream = bundler.bundle();
    return stream
      .on('error', handleErrors)
      .pipe(source(file))
      .pipe(rename('main_min.js'))
      .pipe(buffer())
      .pipe(uglify())
      .pipe(gulp.dest('./js/'))
      .pipe(reload({stream:true}));
  }

  // listen for an update and run rebundle
  bundler.on('update', function() {
    rebundle();
    gutil.log('Rebundle...');
  });

  // run it once the first time buildScript is called
  return rebundle();
}

gulp.task('scripts', function() {
  return buildScript('main.js', true); // this will run once because we set watch to false
});

gulp.task('serve', ['compass', 'scripts'], function() {

    browserSync.init({
        server: "./"
    });

    gulp.watch("./sass/*.scss", ['compass']);
    gulp.watch("./*.html").on('change', browserSync.reload);
    gulp.watch("./js/*.js").on('change', browserSync.reload);
});

// Default
gulp.task('default', ['compass', 'scripts', 'serve']);
