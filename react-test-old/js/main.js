var React = require('react');
var ReactDOM = require('react-dom');

var StorePicker = React.createClass({
  render : function() {
    var name = "Bill";
    return (
      <form className="store-selector">
        {/* comments go here */}
          <h2>Please enter a store {name}</h2>
          <input type="text" ref="storeId" required />
          <input type="submit" />
      </form>
    )
  }
});

ReactDOM.render(<StorePicker/>, document.querySelector('#main'));
