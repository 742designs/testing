<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lynda-wp-custom-fields');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'yGq$*%Vn71HG8-!Y[6e[d,;1i`V(p)&-T@}PrpK;!R~QI|FpMwK|pT]`}ZXB8]du');
define('SECURE_AUTH_KEY',  '1~Z$CZTDC@jl7LO#yKFDC87M8HBVThz1 >YPrs>KfZm.S=N3%Y|P|<520pKpRk+.');
define('LOGGED_IN_KEY',    '/EQ io^uS]|a?v4:u>|3{zHFRC8b1kquq^AtL-lu:cjVS#P>Z>(05;Gt?*_GG%T[');
define('NONCE_KEY',        'yf>~S}NCX_+_tyCh.)g|G8aN|S(HUSM:mye~^Eqsns0|}/5j&_}g)iXJP``8B>=j');
define('AUTH_SALT',        '-S)rC(/#+e3s;OtO#u,uI$h;WnTzAFhE?k#Ib7czO;w8 R9`^.y705aU4LF#b;)8');
define('SECURE_AUTH_SALT', ']dp5;k|xEar{Eo^?6ta<OZk(A+36+!7#{;S]-T<Ycd+[R-SbP Jx!Z=b3a}-UU99');
define('LOGGED_IN_SALT',   'T3d1A.LtH#Uh59]COyVeYB61AISIF,5XFl^ldI|33[Db@Gd+],G~mi?;Tj2G/$)%');
define('NONCE_SALT',       'xWO< ->I|]f1+n4*6+JE)=y?)M|sAS(A#+JEjLq-P}5Tj$@b3nP^,@9<O]MSMtX,');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
