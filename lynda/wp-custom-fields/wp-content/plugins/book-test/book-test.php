<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://github.com/bill742/
 * @since             1.0.0
 * @package           Book_Test
 *
 * @wordpress-plugin
 * Plugin Name:       Book Test
 * Plugin URI:        https://github.com/bill742/wp-plugin-test
 * Description:       Creates "Book" custom post type with shortcodes to use on all posts.
 * Version:           1.0.0
 * Author:            Bill Dean
 * Author URI:        https://github.com/bill742/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       book-test
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-book-test-activator.php
 */
function activate_book_test() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-book-test-activator.php';
	Book_Test_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-book-test-deactivator.php
 */
function deactivate_book_test() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-book-test-deactivator.php';
	Book_Test_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_book_test' );
register_deactivation_hook( __FILE__, 'deactivate_book_test' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-book-test.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_book_test() {

	$plugin = new Book_Test();
	$plugin->run();

}
run_book_test();
