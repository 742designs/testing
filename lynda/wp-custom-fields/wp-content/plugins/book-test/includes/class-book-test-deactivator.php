<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://github.com/bill742/
 * @since      1.0.0
 *
 * @package    Book_Test
 * @subpackage Book_Test/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Book_Test
 * @subpackage Book_Test/includes
 * @author     Bill Dean <742designs@gmail.com>
 */
class Book_Test_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
