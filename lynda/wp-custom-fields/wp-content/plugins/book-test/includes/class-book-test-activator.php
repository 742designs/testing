<?php

/**
 * Fired during plugin activation
 *
 * @link       https://github.com/bill742/
 * @since      1.0.0
 *
 * @package    Book_Test
 * @subpackage Book_Test/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Book_Test
 * @subpackage Book_Test/includes
 * @author     Bill Dean <742designs@gmail.com>
 */
class Book_Test_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
