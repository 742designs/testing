<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://github.com/bill742/
 * @since      1.0.0
 *
 * @package    Book_Test
 * @subpackage Book_Test/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap">

    <h2><?php echo esc_html(get_admin_page_title()); ?></h2>

	<form method="post" name="add-book" action="options.php">
    
    	<?php settings_fields($this->plugin_name); ?>
    	
        <fieldset>
            <legend class="screen-reader-text"><span>Add Book Title</span></legend>
            <label><span><?php esc_attr_e('Book Title', $this->plugin_name); ?></span></label>
            <input type="test" id="book-title" name="book-title" placeholder="Book Title"/>

            <legend class="screen-reader-text"><span>Add Book Publisher</span></legend>
            <label><span><?php esc_attr_e('Book Publisher', $this->plugin_name); ?></span></label>
            <input type="test" id="book-publisher" name="book-publisher" placeholder="Book Publisher"/>
        </fieldset>

        <?php submit_button('Add New Book', 'primary','submit', TRUE); ?>

    </form>

</div>