<?php
/*
Plugin Name: Global Test Plugin
Plugin URI: https://github.com/bill742/
Description: Add cutom post type to input book information.
Version: 0.1
Author: Bill Dean
Author URI: https://github.com/bill742/
License: GPL-2.0+
License URI: License URI: http://www.gnu.org/licenses/gpl-2.0.txt
*/

// Books Custom Post Type Function

function custom_post_type() {

  // UI labels for Books Posts
  $labels = array(
    'name'                => _x( 'Books', 'Post Type General Name' ),
    'singular_name'       => _x( 'Book', 'Post Type Singular Name' ),
    'menu_name'           => __( 'Books'),
    'parent_item_colon'   => __( 'Parent Book' ),
    'all_items'           => __( 'All Books' ),
    'view_item'           => __( 'View Book' ),
    'add_new_item'        => __( 'Add New Book' ),
    'add_new'             => __( 'Add New' ),
    'edit_item'           => __( 'Edit Book' ),
    'update_item'         => __( 'Update Book' ),
    'search_items'        => __( 'Search Book' ),
    'not_found'           => __( 'Not Found' ),
    'not_found_in_trash'  => __( 'Not found in Trash' ),
  );

  // Set options Books Posts

  $args = array(
  	'label'               => __( 'books' ),
  	'description'         => __( 'Books and publishers' ),
  	'labels'              => $labels,
  	'supports'            => array( 'title'),
  	'hierarchical'        => false,
  	'public'              => true,
  	'show_ui'             => true,
  	'show_in_menu'        => true,
  	'show_in_nav_menus'   => true,
  	'show_in_admin_bar'   => true,
  	'menu_position'       => 6,
  	'can_export'          => true,
  	'has_archive'         => true,
  	'exclude_from_search' => false,
  	'publicly_queryable'  => true,
  	'capability_type'     => 'page',
  );

  // Registering Books Posts
  register_post_type( 'books', $args );

}

add_action( 'init', 'custom_post_type', 0 );



// Publisher field
function add_books_meta_boxes() {
	add_meta_box("books_contact_meta", "Add Publisher Information", "add_publisher_books_meta_box", "books", "normal", "low");
}
function add_publisher_books_meta_box() {
	global $post;
	$custom = get_post_custom( $post->ID );

  echo "<input type='text' name='publisher' id='publisher' value='";
  echo @$custom["publisher"][0];
  echo "'/>";

  // Display the shortcode to use once a publisher is added
  $value = get_post_meta($post->ID, 'publisher', true);
  if (!empty($value)) {
    echo "<p><strong>Use the following shortcode to embed this book in your posts:</strong></p> [book id='" . $post->ID . "']";
  }

}
//Save custom field data when creating/updating posts

function save_books_custom_fields() {
  global $post;

  if ( $post ) {
    update_post_meta($post->ID, "publisher", @$_POST["publisher"]);
  }
}
add_action( 'admin_init', 'add_books_meta_boxes' );
add_action( 'save_post', 'save_books_custom_fields' );

// Generate Shortcode
function books_shortcode( $atts ) {

  extract( shortcode_atts(
    array(
      'id' => '',
    ), $atts )
  );

  $page = get_post($id);
  $title = $page->post_title;
  $publisher = $page->publisher;

  return $title . ' published by ' . $publisher . '.';
}
add_shortcode( 'book', 'books_shortcode' );

?>


