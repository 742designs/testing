<?php
/*
Plugin Name: Global Test Plugin
Plugin URI: https://github.com/bill742/
Description: Add cutom post type to input book information.
Version: 0.1
Author: Bill Dean
Author URI: https://github.com/bill742/
License: GPL-2.0+
License URI: License URI: http://www.gnu.org/licenses/gpl-2.0.txt
*/

add_action( 'wp_enqueue_scripts', 'register_plugin_styles' );

function register_plugin_styles() {
  wp_register_style( 'global-test', plugins_url( 'global-test/css/plugin-style.css' ) );
  wp_enqueue_style( 'global-test' );
}

require_once 'php/custom-post-type.php';
require_once 'php/custom-fields.php';
require_once 'php/shortcodes.php';

?>
