<?php

// Add Publisher field Meta box
function add_books_meta_boxes() {
  add_meta_box("books_contact_meta", "Add Publisher Information", "add_publisher_books_meta_box", "books", "normal", "low");
}

function add_publisher_books_meta_box() {
  global $post;
  $custom = get_post_custom( $post->ID );

  echo "<input type='text' name='publisher' id='publisher' value='";
  echo @$custom["publisher"][0];
  echo "'/>";

  // Display the shortcode to use once a publisher is added
  $value = get_post_meta($post->ID, 'publisher', true);
  if (!empty($value)) {
    echo "<p><strong>Copy and paste the following shortcode to embed this book in your posts:</strong></p> [book id='" . $post->ID . "']";
  }
}

//Save custom field data when creating/updating posts
function save_books_custom_fields() {
  global $post;

  if ( $post ) {
    update_post_meta($post->ID, "publisher", @$_POST["publisher"]);
  }
}
add_action( 'admin_init', 'add_books_meta_boxes' );
add_action( 'save_post', 'save_books_custom_fields' );

?>