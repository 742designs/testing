<?php

// Books Custom Post Type Function

function custom_post_type() {

  // UI labels for Books Posts
  $labels = array(
    'name'                => _x( 'Books', 'Post Type General Name' ),
    'singular_name'       => _x( 'Book', 'Post Type Singular Name' ),
    'menu_name'           => __( 'Books'),
    'parent_item_colon'   => __( 'Parent Book' ),
    'all_items'           => __( 'All Books' ),
    'view_item'           => __( 'View Book' ),
    'add_new_item'        => __( 'Add New Book' ),
    'add_new'             => __( 'Add New' ),
    'edit_item'           => __( 'Edit Book' ),
    'update_item'         => __( 'Update Book' ),
    'search_items'        => __( 'Search Book' ),
    'not_found'           => __( 'Not Found' ),
    'not_found_in_trash'  => __( 'Not found in Trash' ),
  );

  // Set options Books Posts

  $args = array(
  	'label'               => __( 'books' ),
  	'description'         => __( 'Books and publishers' ),
  	'labels'              => $labels,
  	'supports'            => array( 'title'),
  	'hierarchical'        => false,
  	'public'              => true,
  	'show_ui'             => true,
  	'show_in_menu'        => true,
  	'show_in_nav_menus'   => true,
  	'show_in_admin_bar'   => true,
  	'menu_position'       => 6,
  	'can_export'          => true,
  	'has_archive'         => true,
  	'exclude_from_search' => false,
  	'publicly_queryable'  => true,
  	'capability_type'     => 'page',
  );

  // Registering Books Posts
  register_post_type( 'books', $args );

}

add_action( 'init', 'custom_post_type', 0 );

?>