<?php

// Generate Shortcode
function books_shortcode( $atts ) {

  extract( shortcode_atts(
    array(
      'id' => '',
    ), $atts )
  );

  $page = get_post($id);
  $title = $page->post_title;
  $publisher = $page->publisher;

  return '<p class="book-box"><em>' . $title . '</em> published by <strong>' . $publisher . '</strong>.</p>';
}
add_shortcode( 'book', 'books_shortcode' );

?>