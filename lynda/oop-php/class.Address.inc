<?php

class Address {
    public $street_address_1;
    public $street_address_2;
    public $city_name;
    public $subdivision;
    public $postal_code;
    public $country_name;

    // Primary key of Address
    protected $_address_id;

    // When last created and updated
    protected $_time_created;
    protected $_time_updated;

    function display() {
        $output = '';

        $output .= $this->street_address_1;
        if ($this->street_address_2){
            $output .= '<br>' . $this->street_address_2;
        }

        $output .= '<br>';
        $output .= $this->city_name . ', ' . $this->subdivision;
        $output .= ' ' . $this->postal_code;
        $output .= '<br>';
        $output .= $this->country_name;

        return $output;
    }
}

?>
