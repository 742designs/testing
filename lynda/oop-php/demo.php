<?php

    require 'class.Address.inc';

    echo '<h2>Address</h2>';
    $address = new Address;

    echo '<h2>Empty Address</h2>';
    echo '<tt><pre>' . var_export($address, TRUE) . '</pre></tt>';

    echo '<h2>setting properties</h2>';
    $address->street_address_1 = '555 Fake St';
    $address->city_name = 'Townsville';
    $address->subdivision = 'State';
    $address->postal_code = "12345";
    $address->country_name = "Canada";
    echo '<tt><pre>' . var_export($address, TRUE) . '</pre></tt>';

    echo '<h2>display Address</h2>';
    echo $address->display();

    echo '<h2>Testing protected access';
    echo "Address ID: {$address->_address_id}";
