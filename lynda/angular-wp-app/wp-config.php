<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lynda-angular-wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'cNfuhc[4{B](0WV{p2Y7iPi/@3Bj|+CsaH@tj#*_x ?{6h@|ZvOr86$SapKDaIJO');
define('SECURE_AUTH_KEY',  'OzvoZ)t[)[zxae7fzb(QkN*>7KNEc6v<;C6@gCiY^X.|S?`5Vd0-8J4ykS[Hg1M|');
define('LOGGED_IN_KEY',    '%wQGoy#5##Y7~Q5eRQsu4v5ja&1WsJ_B:b8`5S?MlWV*K-Nr/}@:S(rQ5s&2F[[U');
define('NONCE_KEY',        'AO5kLcXHnuLke.$YI(->{^Ol7S7M;e0i5R*+gP`e7@`8sUNgD+|2}-h10?$a{*<N');
define('AUTH_SALT',        '!Kv-b rBbq]7tZ*z|4Wi+68-cD6$}s+e3-EN=<_EKNQMx//0,pO6hRf[SQ~=RQWA');
define('SECURE_AUTH_SALT', 'Ybc3L>)[{I7=PWRljt/&wc7,}Si(0|3GR$%cI*to4n7lD.G&%PT_6uos6^#lElh3');
define('LOGGED_IN_SALT',   '{+:0#Cm`;cNSJnJX?Wa`102^gBcnT)l`fA${qkf@D^eyzhn:Hfr}ghi^-ABT,Cra');
define('NONCE_SALT',       'Xv$rL+MT-ME;Ot2RtG9+HV:_k#JsA%0%G6:P>wqO:9hUcYlhVcG$&hm!8lH|bU# ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
