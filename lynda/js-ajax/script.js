var mybutton = document.getElementById('loadbutton');
mybutton.onclick = loadAJAX;

function loadAJAX(){

	var request;

	// Check for older browsers
	if (window.XMLHttpRequest){
		request = new XMLHttpRequest();
	} else {
		request = new ActiveXObject("Microsoft.XMLHTTP");
	}

	// Load data.txt
	request.open('GET', 'data.json');


	request.onreadystatechange = function(){
		if ((request.readyState===4) && (request.status===200)) {
			//console.log( request );
			//document.writeln(request.responseText);
			
			//var modify = document.getElementsByTagName('li');
			// for (var i = 0; i < modify.length; i++) {
			// 	modify[i].innerHTML = request.responseText;
			// }

			// To display data.xml
			// console.log( request.responseXML.getElementsByTagName('name')[1].firstChild.nodeValue );

			// var items = request.responseXML.getElementsByTagName('name');

			// var output = '<ul>';
			// for (var i = 0; i < items.length; i++) {
			// 	output += '<li>' + items[i].firstChild.nodeValue + '</li>';
			// }
			// output += '</ul>';
			// document.getElementById('update').innerHTML = output;

			//To display Data.json
			var items = JSON.parse(request.responseText);
			//console.log( items );
			var output = '<ul>';
			for (var key in items) {
				output += '<li>' + items[key].name + '</li>';
			}
			output += '</ul>';
			document.getElementById('update').innerHTML = output;
		}
	};
	request.send();

}
