(function(){
	var app = angular.module('store', []);	

	app.controller('StoreController', function(){
		this.products = gems;
	});

	var gems = [
		{
			name: 'Dodecahedron',
			price: 2,
			description: '...',
			canPurchase: true,
			reviews: [
				{
					stars: 5,
					body: "It's awesome!",
					author: "joe@test.com"
				},
				{
					stars: 1,
					body: "It sucks!",
					author: "joe2@test2.com"
				}
			],
			images: [
				{
					full: 'dodecahedron-01-full.jpg',
					thumb: 'dodecahedron-01-thumb.jpg',
				},
				{
					full: 'dodecahedron-02-full.jpg',
					thumb: 'dodecahedron-02-thumb.jpg',
				}
			]
		},
		{
			name: 'Pentagonal Gem',
			price: 5.95,
			description: '...',
			canPurchase: false,
			reviews: [
				{
					stars: 5,
					body: "It's awesome!",
					author: "joe@test.com"
				},
				{
					stars: 1,
					body: "It sucks!",
					author: "joe2@test2.com"
				}
			]
		}
	];

	app.controller("PanelController", function(){
		this.tab = 1;

		this.selectTab = function(setTab){
			this.tab = setTab;
		};
		this.isSelected = function(checkTab){
			return this.tab === checkTab;
		};
	});

	app.controller("ReviewController", function(){
		this.review = {};

		this.addReview = function(product) {
			product.reviews.push(this.review);
			this.review = {};
		};
	});

	app.directive('productTitle', function(){
		return {
			restrict: 'E',
			templateUrl: 'product-title.html'
		};
	});

})();