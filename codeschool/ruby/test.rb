t = Tweet.find(3)

/app/models/tweet.rb

class Tweet < ActiveRecord::Base
    # validates_presence_of :status
    validates :status,
                presence: true,
                length {minimum: 3}
    belongs_to :zombie
end

class Zombie < ActiveRecord::Base
    has_many :tweets
end


t.save

t.errors.messages
t.errors[:status][0]



ash = Zombie.find(1)
t = Tweet.create(status: "My tweet here",
                    zombie: ash)
ash.tweets.count # equals 3
ash.tweets
