# Describe the sketch comedy group

first_name = 'Monty'
last_name = 'Python'
full_name = first_name + ' ' + last_name
description = 'sketch comedy group'
year = 1969
sketch1 = '\n\tHell\'s Grannies'
sketch2 = '\n\tThe Dead Parrot'

sentence = full_name + ' is a ' + description + ' formed in ' + str(year)

print(sentence)

print('Famous Works:', sketch1, sketch2)
