import requests

def get_weather_forecast():
    # connecting to weather API
    url = 'http://api.openweathermap.org/data/2.5/weather?q=Orlando,fl&units=imperial&appid=b32c84201db94b92ed42d393cac6526b'
    weather_request = requests.get(url)
    weather_json = weather_request.json()

    # Parsing JSON
    description = weather_json['weather'][0]['description']
    temp_min = weather_json['main']['temp_min']
    temp_max = weather_json['main']['temp_max']

    # Creating forecast string
    forecast = 'The circus forecast for today is '
    forecast += description + ' with a high of ' + str(int(temp_max))
    forecast += ' and a low of ' + str(int(temp_min))

    return (forecast)
