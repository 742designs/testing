import smtplib

def send_emails(emails, schedule, forecast):
    # Connect to the gmail smtp server
    server = smtplib.SMTP('smtp.gmail.com', '587')
    # start TLS encryption
    server.starttls()
    # Login
    password = input("What's your password? ")
    from_email = '742designs@gmail.com'
    server.login(from_email, password)

    # send mail to entire email list
    for to_email, name in emails.items():
        message = 'Subject: Welcome to the circus!\n'
        message += 'Hi ' + name + '!\n\n'
        message += forecast + '\n\n'
        message += schedule + '\n\n'
        server.sendmail(from_email, to_email, message)
    server.quit()
