// for (var i = 10; i > 0; i--) {
//     console.log(i);
// }



var numSheep = 4;
var monthsToPrint = 12;

// while (monthNumber <= monthsToPrint) {
//   numSheep = numSheep * 4;
//   console.log("There will be " + numSheep + " sheep after " + monthNumber + " month(s)!");
//   monthNumber++;
// }

// for (var monthNumber = 1; monthNumber <= monthsToPrint; monthNumber++) {
//     numSheep = numSheep * 4;
//     console.log("There will be " + numSheep + " sheep after " + monthNumber + " month(s)!");
// }


var currentGen = 1;
var totalGen = 19;
var totalMW = 0;

while (currentGen < 5) {
    totalMW = totalMW + 62;
    console.log("Generator #" + currentGen + " is on, adding 62 MW, for a total of " + totalMW + " MW!");
    currentGen++;
}

for (var currentGen = 5; currentGen < 20; currentGen++) {
    totalMW = totalMW + 124;
    console.log("Generator #" + currentGen + " is on, adding 124 MW, for a total of " + totalMW + " MW!");
}
