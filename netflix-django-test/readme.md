Tutorial Link: https://www.youtube.com/watch?v=1TugFJ1sfF8&index=1&list=PLLZsdhESe9KvrqfWk7OhHIk0cYtmdeLPN

Set up environment:

Install virtualenv
pip install virtualenv

Create new project folder and CD into it
mkdir my-new-folder
cd my-new-folder

Make Virtual Environment in Mac/Linux
virtualenv -p /usr/bin/python3 netflix-test-env

Make Virtual Environment in Windows
virtualenv netflix-test-env

Activate Virtual Environment in Mac/Linux
source netflix-test-env/bin/activate

Activate Virtual Environment in Windows
source netflix-test-env/Scripts/activate

Install Django
pip install django

Start Django project
django-admin startproject netflix_test .

Start and create app
python manage.py startapp netflix_app

Run Server
python manage.py runserver
