from django.apps import AppConfig


class NetflixAppConfig(AppConfig):
    name = 'netflix_app'
