(function(){
    'use strict';

    var array = ['one', 'two', 'three'];

    console.log(array.length);

    array.push('four');
    console.log(array);

    array.unshift('zero');
    console.log(array);

    array.pop();
    console.log(array);

    array.shift();
    console.log(array);

    array.splice(0,3,'a','b','c');
    console.log(array);
}());
