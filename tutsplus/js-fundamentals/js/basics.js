

console.log('<3');

var theMeaningOfLife;

var postfix = 5;
var prefix = 5;

var pres = (1 + 4) * 2;
// console.log(pres);

var object = {
    property1: true,
    property2: 'hello'
}

var string = "hello";
var stringObject = new String('An object string');
var stringValue = stringObject.valueOf();
// console.log(stringObject);
// console.log(stringValue);

function getThingByColor(color){
    var things = {
        red: 'a red thing',
        green: 'a green thing',
        blue: 'a blue thing'
    };
    return things[color] || 'Sorry no color exists.'
}

// (function auto(){
//     console.log("automatic!!!");
// }());

console.log(window.location.href);

(function (){
    'use strict';

    var object = {
        property: 'i belong to this',
        method: function(){
            return this.property;
        }
    }
    console.log(object.method());

    function Person(name){
        this.name = name;
    }

    var bob = new Person('Bob');
    console.log(bob.name);
}());
