(function(){
    'use strict';

    var testing = "test string";
    console.log(testing.length);

    console.log(testing.split(' '));
    console.log(testing.indexOf('t', 1));
    console.log(testing.lastIndexOf('t'));
    console.log(testing.toLowerCase());
    console.log(testing.substring(5));
}());
