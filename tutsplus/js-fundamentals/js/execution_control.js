(function(){
    'use strict';

    if (1+1 === 3){
        console.log('it is true.');
    }


    var switcher = "three";

    switch (switcher) {
        case 'one':
            console.log('one');
            break;
        case 'two':
            console.log('two');
            break;
        default:
            //console.log(switcher);
    }

    var anArray = ['item1', 'item2', 'item3'];

    for (var i = 0, x = anArray.length; i < x; i++) {
        //console.log(anArray[i]);
    }

    var anObject = {
        prop1: 'property1',
        prop2: 'property2',
        prop3: 'property3'
    };

    for (var prop in anObject) {
        if (anObject.hasOwnProperty(prop)){
            //console.log(anObject[prop]);
        }
    }

    var limit = 0;
    while (limit < 5) {
        console.log(limit +=1);
        if (limit===3) {
            break;
        }
    }

    var condition = true;
    do {
        console.log('once');
        break;
    } while (condition);

}());
