(function(){
    'use strict';

    function onPositionReceived(position){
        console.log(position);
    }

    function onPositionFailed(positionError){
        console.log(positionError);
    }

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(onPositionReceived, onPositionFailed, {
            timeout: 0
        });
    }
}());
