import React from 'react';
import Header from './Header';
import Content from './Content';

class App extends React.Component {
  render() {
    return  (
      <div className="wrapper">
        <Header tagline="My cool react app" />
        <Content />
      </div>
    )
  }
}

export default App;
