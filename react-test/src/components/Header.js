import React from 'react';

const Header = (props) => {
  return (
    <header>
      <h1>Gtr Shop</h1>
      <h2>{props.tagline}</h2>
    </header>
  )
}

export default Header;
