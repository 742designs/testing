# Makes a copy of the conf file and updates the URL

# Imports
import os
import shutil
from os import path
os.system('ls')
os.chdir("/bdean")
import sys

#Variables
t = "t."

# Conf file input
confFile = raw_input("Input the conf file to be copied including extension. ")

def main():
    call('ls')
    # make a duplicate of an existing file
    if path.exists(confFile):
      # get the path to the file in the current directory
      src = path.realpath(confFile);
      # separate the path part from the filename
      head, tail = path.split(src)
      #print "path: " + head
      #print "file: " + tail
    else:
      # End the script
      sys.exit("File not found!")

    # Make a copy and prepend "t." to the filename
    dst = head + "/" + t + tail
    # make a copy of the file
    shutil.copy(src,dst)
    # copy over the permissions, modification times, and other info
    shutil.copystat(src, dst)

    print "Conf file copied - " + dst

    # find and replace in the new conf file
    newfile = t + tail

    with open(newfile, 'r') as f:
        text = f.read()

    oldUrl = raw_input("Input the url to search for: ")
    newUrl = raw_input("Input the new url: ")

    text = text.replace(oldUrl, newUrl)

    with open(newfile, 'w') as f:
        f.write(text)

    print newfile + " has been updated"

if __name__ == "__main__":
  main()
