<?php
$total = 0;
foreach(explode("\n", file_get_contents("input.txt")) as $line) {
    $a = explode('x', $line);
    $areas = array($a[0]*$a[1]*2, $a[1]*$a[2]*2, $a[0]*$a[2]*2);
    sort($areas);
    $total += array_sum($areas) + ($areas[0]/2);
}
print "$total\n";