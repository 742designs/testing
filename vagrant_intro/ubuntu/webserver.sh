#!/usr/bin/env bash
apt-get install apache2 -y
apt-get update
apt-get upgrade -y
rm -rf /var/www/html
ln -fs /vagrant /var/www/html
