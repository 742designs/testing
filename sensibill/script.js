document.getElementById('file').onchange = function(){

  var file = this.files[0];

  var reader = new FileReader();
  reader.onload = function(progressEvent){

    var words = this.result.split(' ');

    var count = {};
    var output;

    for (var i = 0; i < words.length; i++) {
      var letter = words[i].toLowerCase().charAt(0);

      if (count[letter]) {
        count[letter]++;
      } else {
        count[letter] = 1;
      }
    }

    var totals = JSON.stringify(count);

    output = '<p>There are ' + i + ' words.</p>';
    output += '<p>' + totals + '</p>';
    document.getElementById('counter-box').innerHTML = output;

    console.log(JSON.stringify(count));

  };
  reader.readAsText(file);
}
